## Folder Structure

```
./src
├── helpers - global helpers
├── hocs - dependency injection
├── pages - all pages declaration
├── routes - routers declaration
├── styles - common styles
├── services -  encapsulated services
├── static - assets and another static project files
├── store - root reducers, root saga and common store logic
│   ├── reducers
│   └── sagas
├── modules - all elements logic
│   └── moduleName - module structure
│       ├── helpers
│       ├── constants
│       ├── store
│       │   ├── actions
│       │   ├── reducers
│       │   └── sagas
│       ├── components
│       │   ├── index.jsx
│       │   └── index.scss
│       ├── index.js
│       └── index.scss
├── index.html
└── index.js

```